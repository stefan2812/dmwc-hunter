local DMW = DMW
DMW.Rotations.HUNTER = {}
local Hunter = DMW.Rotations.HUNTER
local UI = DMW.UI

function Hunter.Settings()
	UI.AddHeader("General")
	UI.AddToggle("Auto Face","Auto face the target, while in combat and standing still",false)
	UI.AddToggle("Auto Attack", "Starts attacking when a valid target is selected and in range", false)
	UI.AddToggle("AutoTarget", "Auto Target next mob while infight", false)
	UI.AddToggle("Use Racial", "Use Racial Abilities", false)

	
	UI.AddHeader("Experimental - BETTER NOT USE AFK")
	UI.AddToggle("AutoMove","Try to get in position to use Melee / Range spells",false)
	UI.AddHeader("Experimental")
	UI.AddRange	("Conserve x% Mana", "Will not cast Attack Spells when under x% Mana", 5, 100, 5, 5)

	UI.AddHeader("Aspect...")
	UI.AddToggle("...of the Hawk", "Will use AotH in combat", false)
	UI.AddToggle("...of The Cheetah / Pack", "Use Aspect of the Cheetah / Pack", false)
	UI.AddToggle("...of The Monkey", "Use Aspect of the Monkey", false,true)
	UI.AddRange	("...of the Monkey HP", "HP to cast AotM", 0, 100, 1, 75,false)
	
	UI.AddHeader("Spells")
	UI.AddToggle("Arcane Shot", "Use Arcane Shot", false)
	UI.AddToggle("Aimed Shot", "Use Aimed Shot", false)
	UI.AddToggle("Hunters Mark", "Use Hunters Mark", false)
	UI.AddToggle("Multishot", "Use Multishot", false)
	UI.AddToggle("Volley", "Use Volley", false)
	UI.AddToggle("RapidFire", "Use RapidFire", false)
	


	UI.AddHeader("Stings")
	UI.AddToggle("Serpent Sting", "Use Serpent Sting", false)
	UI.AddToggle("Scorpid Sting", "Use Scorpid Sting", false)
	UI.AddToggle("Viper Sting", "Use Viper Sting", false)

	UI.AddHeader("Pet Management")
	UI.AddToggle("Auto Pet Attack", "Auto cast pet attack on target", false)
	UI.AddToggle("Call Pet", "Call active pet", false)
	UI.AddToggle("Feed Pet", "Requires a Feed_Pet macro (/use FOODNAME)", false)
	UI.AddToggle("Revive Pet", "Activate Revive Pet", false)
	UI.AddToggle("Intimidation", "Use Intimidation",false)
	UI.AddToggle("Auto Manage Growl", "Turns Growl On / Off for Solo / Dungeon play",false)
	UI.AddToggle("Mend Pet", "Activate Mend Pet", false)
	UI.AddRange	("Mend Pet HP", "Pet HP to cast Mend Pet", 0, 100, 1, 35)


	UI.AddHeader("Consumables")
	UI.AddToggle("Use HP Potion",nil,false,true)
	UI.AddDropdown("HP Potion to use", "Use HP Potion", {"Minor Healing Potion","Lesser Healing Potion","Healing Potion","Greater Healing Potion","Superior Healing Potion", "Major Healing Potion"}, "1",true)
	UI.AddRange("Use Potion at #% HP", nil, 10, 100, 5, 50, true)
end