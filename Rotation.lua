local DMW = DMW
local Hunter = DMW.Rotations.HUNTER
local Rotation = DMW.Helpers.Rotation
local Setting = DMW.Helpers.Rotation.Setting
local Player, Pet, Buff, Debuff, Spell, Target, Talent, Item, GCD, Health, CDs, HUD, Happiness, DamagePercentage, LoyaltyRate
local ShotTime = GetTime()
local foodTime
local CalledTime
local Called = false
local AutoMoveM = false
local AutoMoveR = false
local inInstance, instanceType
local race = UnitRace("player");
local function Locals()
    Player = DMW.Player
    Buff = Player.Buffs
    Debuff = Player.Debuffs
	Health = Player.Health
	Pet = DMW.Player.Pet
    HP = Player.HP
    Power = Player.PowerPct
    Spell = Player.Spells
    Talent = Player.Talents
    Trait = Player.Traits
    Item = Player.Items
	Target = Player.Target or false
	if Pet then
		PetTarget = Pet.Target or false
	end
    HUD = DMW.Settings.profile.HUD
    CDs = Player:CDs()
	GCD = Player:GCD()
	Happiness, DamagePercentage, LoyaltyRate = GetPetHappiness()
	inInstance, instanceType = IsInInstance()
	powerType, powerTypeString = UnitPowerType("target");
	autocastable, growlstate = GetSpellAutocast("Growl", pet)
end
local function AutoAttack()
	if  not IsAutoRepeatSpell(Spell.AutoShot.SpellName) and (DMW.Time - ShotTime) > 0.5 and Target.Distance >= 9 then
		if Spell.AutoShot:Cast(Target) then
			ShotTime = DMW.Time
			return true
		end
	end
	
	if Target and Target.ValidEnemy and not IsCurrentSpell(6603) and Target.Distance <= 8 and Target:IsEnemy() then
		if  IsAutoRepeatSpell(Spell.AutoShot.SpellName) then
			Spell.AutoShot:Cast(Target)
		end
		StartAttack(Target.Pointer)
	end
end
local function AutoTarget()
	-- Auto Targeting --
	if Player.Combat and not (Target and Target.ValidEnemy) and #Player:GetEnemies(34) >= 1 and Setting("AutoTarget") then
		TargetUnit(DMW.Attackable[1].unit)
	end
end
local function Potionuse()
	PotionName = Setting("HP Potion to use")
	if PotionName == 1 then
		PotionID = 118
	end
	if PotionName == 2 then
		PotionID = 858
	end
	if PotionName == 3 then
		PotionID = 929
	end
	if PotionName == 4 then
		PotionID = 1710
	end
	if PotionName == 5 then
		PotionID = 3928
	end
	if PotionName == 6 then
		PotionID = 13446
	end
end
local function UsePotion()
	if Setting("Use HP Potion") and not Setting("Use Best HP Potion available") then
		if GetItemCount(PotionID) >= 1 and GetItemCooldown(PotionID) == 0 then
			if HP <= Setting("Use Potion at #% HP") and Player.Combat then
				name = GetItemInfo(PotionID)
				RunMacroText("/use " .. name)
				return true
			end
		end
	end
end
local function PetManagement()
	if Happiness == nil then
		if Pet then
			Happiness, DamagePercentage, LoyaltyRate = GetPetHappiness()
		else
			Happiness = 0
		end
	end
	-- Step 1: Call Pet
	if Setting("Call Pet") and not Pet and not Called then
		if CalledTime == nil or CalledTime <= DMW.Time then
			if Spell.CallPet:Cast(Player) then
				return true 
			end
			Called = true
			CalledTime = DMW.Time + 3
		end
	end
	-- Step 2: Revive pet after Call Pet failed
	if Setting("Revive Pet") and Called and not Pet then
		if Spell.RevivePet:Cast(Player) then
			return true
		end
	end
	if not CalledTime == nil and CalledTime > DMW.Time then
		Called = false
	end
    -- Step 3: Revive pet after it died
	if Setting ("Revive Pet") and Pet and Pet.Dead then
		if Spell.RevivePet:Cast(Player) then
			return true
		end
	end
	-- Step 4: If pet was Revived after failed call, reset checks
	if Pet then
		Called = false
	end
	-- Step 5: Mend pet
	if Setting("Mend Pet") and Player.Combat and not Player.Moving and Pet and not Pet.Dead and Pet.HP < Setting("Mend Pet HP") and Player.PowerPct > 30 and (inInstance == false or inInstance == nil) then
		if Spell.MendPet:Cast(Pet) then
			return true
		end
	end
	-- Step 6: If pet is not Happy: feed it
	if Setting("Feed Pet") and Pet and not Pet.Dead and Happiness < 3 and not Pet:AuraByID(1539) and not Player.Combat then
		if foodTime == nil or foodTime <= DMW.Time then
			if Spell.FeedPet:Cast(Player) then
				RunMacro("Feed_Pet")
				Happiness, DamagePercentage, LoyaltyRate = GetPetHappiness()
				foodTime = DMW.Time + 2
			end
		end
	end	
end
local function Aspects()
	-- Aspect of the Cheetah
	if not UnitInParty("player") or not Spell.AspectOfThePack:Known() then
		if Spell.AspectOfTheCheetah:IsReady() and Setting("...of The Cheetah / Pack") and not Player.Combat and not Spell.AspectOfTheHawk:LastCast() and Player.Moving and not Buff.AspectOfTheCheetah:Exist(Player) and #Player:GetEnemies(8) == 0 then
			if  Spell.AspectOfTheCheetah:Cast(Player) then
				return true
			end
		end
		if Spell.AspectOfTheCheetah:IsReady() and Buff.AspectOfTheCheetah:Exist(Player) and (#Player:GetEnemies(8) >= 1 or Player.Combat) then
			if  Spell.AspectOfTheCheetah:Cast(Player) then
				return true
			end
		end
	end
	-- Aspect of the Pact --
	if UnitInParty("player") and Spell.AspectOfThePack:Known() then
		if Spell.AspectOfThePack:IsReady() and Setting("...of The Cheetah / Pack") and not Player.Combat and not Spell.AspectOfTheHawk:LastCast() and Player.Moving and not Buff.AspectOfThePack:Exist(Player) and #Player:GetEnemies(8) == 0 then
			if  Spell.AspectOfThePack:Cast(Player) then
				return true
			end
		end
		if Spell.AspectOfThePack:IsReady() and Buff.AspectOfThePack:Exist(Player) and (#Player:GetEnemies(8) >= 1 or Player.Combat) then
			if  Spell.AspectOfThePack:Cast(Player) then
				return true
			end
		end
	end
	-- Aspect of the Hawk --
	if Spell.AspectOfTheHawk:IsReady() and Setting("...of the Hawk") and Target and not Target.Friend and Target.Distance > 8 and not Buff.AspectOfTheHawk:Exist(Player) and Player.PowerPct > 30 and Player.HP > Setting("...of the Monkey HP") then
		if  Spell.AspectOfTheHawk:Cast(Player) then
			return true
		end
	end	 
	--Aspect of the Monkey   
	if  Spell.AspectOfTheMonkey:IsReady() and Setting ("...of The Monkey") and Target and Player.Combat and Player.HP < Setting("...of the Monkey HP") and Player.PowerPct > 20  and Target.Distance < 8 and not Buff.AspectOfTheMonkey:Exist(Player) then
		if Spell.AspectOfTheMonkey:Cast(Player) then
			return true 
		end
	end
end
local function Facing()
    if Player.Combat and Target and not Target.Facing and not Player.Moving then
		FaceDirection(Target.Pointer, true)
    end
end
local function Racial()
	if Setting("Use Racial") then
		if race == "Tauren" and Player.Combat and #Player:GetEnemies(8) >= 2 and Spell.WarStomp:IsReady() then
			if Spell.WarStomp:Cast(Player) then
				return true
			end
		elseif race == "Orc" and Spell.BloodFury:IsReady() and Player.Combat and Target and Target.TTD > 2 then
			if Spell.BloodFury:Cast(Player) then
				return true
			end
		elseif race == "Undead" and Player:HasFlag(DMW.Enums.UnitFlags.Feared) and Spell.WillofTheForsaken:IsReady() then
			if Spell.BloodFury:Cast(Player) then
				return true
			end
		elseif race == "Troll" and Spell.BerserkingTroll:IsReady() and Player.Combat and Target then
			if Spell.BerserkingTroll:Cast(Player) then
				return true
			end
		end
	end
end
local function AutoMoveMelee()
	if Target and not Target.Dead and not Target.Friend then
		if Target.Distance > 1 and not IsHackEnabled("follow") then
			RunMacroText(".follow 1")
			AutoMoveM = true
		end
	else
		if IsHackEnabled("follow") then
			RunMacroText(".follow")
			AutoMoveM = false
		end
	end
end
local function AutoMoveRange()
	if Target and not Target.Dead and not Target.Friend then
		if not IsHackEnabled("follow") then
			RunMacroText(".follow 17")
			AutoMoveR = true
		end
	else
		if IsHackEnabled("follow") then
			RunMacroText(".follow")
			AutoMoveR = false
		end
		if IsHackEnabled("follow") and IsSpellInRange("Arcane Shot") then
			RunMacroText(".follow")
			AutoMoveR = false
		end
	end
end

local function Sting()
	-- Serpent Sting --
	if Setting("Serpent Sting") and Target.Distance > 8 and Target.TTD > 5 and not (Target.CreatureType == "Mechanical" or Target.CreatureType == "Elemental" or CreatureType == "Totem") and not Debuff.SerpentSting:Exist(Target) then
		if not Debuff.ViperSting:Exist(Target) then
			if Spell.SerpentSting:Cast(Target) then
				return true
			end
		end
	end
	-- Scorpid Sting --
	if Setting("Scorpid Sting") and not Setting("Serpent Sting") and Target.Distance > 8 and Target.TTD > 5 and not (Target.CreatureType == "Mechanical" or Target.CreatureType == "Elemental" or CreatureType == "Totem") and not Debuff.ScorpidSting:Exist(Target)  then
		if not Debuff.ViperSting:Exist(Target) and Player.PowerPct > mana2 and powerTypeString == "MANA" then
			if Spell.ScorpidSting:Cast(Target) then
				return true
			end
		elseif Debuff.ViperSting:Exist(Target) and not powerTypeString == "MANA" then
			if Spell.ScorpidSting:Cast(Target) then
				return true
			end
		end
	end
	-- Viper Sting --
	if Setting("Viper Sting") and not Setting("Serpent Sting") and powerTypeString == "MANA" and Target.Distance > 8 and not (Target.CreatureType == "Mechanical" or Target.CreatureType == "Elemental" or CreatureType == "Totem") and not Debuff.ViperSting:Exist(Target) then
		if Spell.ViperSting:Cast(Target) then
			return true
		end
	end
end
local function CombatSolo()
	if (not Target or Target.Dead) and IsHackEnabled("follow") then
		RunMacroText(".follow")
	end
	if IsHackEnabled("follow") and AutoMoveR and Player.IsTanking() then
		RunMacroText (".follow")
		AutoMoveR = false
	end
	if IsHackEnabled("follow") and AutoMoveM and not Player.IsTanking() then
		RunMacroText (".follow")
		AutoMoveM = false
	end
	if Player.Combat and not Target and Pet and Pet.Target then
		AssistUnit("pet")
	end
	-- AutoMove Melee --
	if Target and not Target.Friend and Setting("AutoMove") and not AutoMoveR and Player:IsTanking(Target) then
		AutoMoveMelee()
	end
	-- AutoMove Range --
	if Target and not Target.Friend and Setting("AutoMove") and not AutoMoveM and not Player:IsTanking(Target) then
		AutoMoveRange()
	end
	-- Auto Target --
	if Player.Combat and not Target and Setting("AutoTarget") then
		AutoTarget()
	end
	if Target and not Target.Dead and not Target.Friend and (Player.Combat or Setting("Auto Attack"))then
		-- Pet Auto	--
		if Setting("Auto Pet Attack") and Target and Pet and not Pet.Dead and not UnitIsUnit(Target.Pointer, "pettarget") and (Setting("Auto Attack") or Player.Combat) and Target.HP >= 1 and not UnitIsTapDenied(Target.Pointer) and not Target.Friend then
			PetAttack()
		end
		-- Racial --
		if Racial() then
			return true
		end
		-- Rapidfire --
		if Setting("RapidFire") and Spell.RapidFire:IsReady() and Player.Combat and Target.TTD >= 4 then
			if Spell.RapidFire:Cast() then
				return true
			end
		end 
		-- Volley --
		if Setting("Volley") and Target.Distance > 8 and #Target:GetEnemies(8) >= 2 and Spell.Volley:IsReady() and Power >= Setting("Conserve x% Mana") then
			if Spell.Volley:CastGround(Target.PosX, Target.PosY, Target.PosZ) then
				return true
			end
		end
		-- Intimidation --
		if Setting("Intimidation") and Spell.Intimidation:IsReady() and Player:IsTanking(Target) and Power >= Setting("Conserve x% Mana") then
			if Spell.Intimidation:Cast(Player) then
				return trued
			end
		end
		-- Bestial Wrath --
		if Spell.BestialWrath:IsReady() and Pet and Power >= Setting("Conserve x% Mana") then
			Spell.BestialWrath:Cast(Pet)
		end
		-- Hunter's Mark --
		if Setting("Hunters Mark") and Target.Distance > 8 and Target.TTD > 2 and not Debuff.HuntersMark:Exist(Target) and not (Target.CreatureType == "Totem") and Power >= Setting("Conserve x% Mana") then
			if Spell.HuntersMark:Cast(Target) then
				return true
			end
		end 		
		-- Auto Attack --	
		AutoAttack() 	
		-- Stings --
		if Power >= Setting("Conserve x% Mana") then
			if Sting() then
				return true
			end
		end
		--Concussive Shot --
		if Player:IsTanking(Target) and Target.TTD >= 3 and Spell.ConcussiveShot:IsReady() and Target.Distance >= 8 and Target.Distance <= 35 and Power >= Setting("Conserve x% Mana") then
			if Spell.ConcussiveShot:Cast(Target) then
				return true
			end
		end	
		-- MultiShot --
		if Setting("Multishot") and Target.Distance > 8 and Spell.MultiShot:IsReady() and not (Target.CreatureType == "Totem") and Power >= Setting("Conserve x% Mana") then
			if #Target:GetEnemies(8) >= 2 then 
				if Spell.MultiShot:Cast(Target) then
					return true
				end
			end
		end
		-- Arcane Shot --
		if Setting("Arcane Shot") and not Setting("Aimed Shot") and Target.Distance > 8 and Spell.ArcaneShot:IsReady() and Target.TTD > 4 and not (Target.CreatureType == "Totem") and Power >= Setting("Conserve x% Mana") then
			if Spell.ArcaneShot:Cast(Target) then
				return true
			end
		end
		-- Aimed Shot --
		if Setting("Aimed Shot") and Target.Distance > 8 and Spell.AimedShot:IsReady() and Target.TTD > 4 and not (Target.CreatureType == "Totem") and not IsCurrentSpell(19434) and Power >= Setting("Conserve x% Mana") then
			if Spell.AimedShot:Cast(Target) then
				return true
			end
		end
		--Raport Strike --
		if Target.Distance < 5 and Target.TTD > 2 and not IsAutoRepeatSpell(Spell.AutoShot.SpellName) and Power >= Setting("Conserve x% Mana") then
			if Spell.RaptorStrike:Cast(Target) then
				return true
			end
		end
		-- Wing Clip --
		if Target.Distance < 5 and not Debuff.WingClip:Exist(Target) and not (Target.CreatureType == "Totem") and Power >= Setting("Conserve x% Mana") then
			if Spell.WingClip:Cast(Target) then
				return true
			end	
		end	
		--Mongoose Bite --
		if Target.Distance < 5  and Spell.MongooseBite:IsReady() and Power >= Setting("Conserve x% Mana") then
			if Spell.MongooseBite:Cast(Target) then 
				return true	
			end
		end
	end
end
local function CombatDungeon()
	-- Auto Target --
	if Player.Combat and not Target and Pet and Pet.Target then
		AssistUnit("pet");
	end
	if Target and not Target.Dead and not Target.Friend and Target.Facing and Player.Combat then
		-- Pet Auto	--
		if Setting("Auto Pet Attack") and Pet and not Pet.Dead and not UnitIsUnit(Target.Pointer, "pettarget") and Target.HP >= 1 and not Target.Friend and Player.Combat and (IsCurrentSpell(6603) or IsAutoRepeatSpell(Spell.AutoShot.SpellName)) then
			PetAttack()
		end
		-- Racial --
		if Player.Combat and Target.TTD >= 4 and Target:IsBoss() then
			if Racial() then
				return true
			end
		end
		-- Rapidfire --
		if Setting("RapidFire") and Spell.RapidFire:IsReady() and Player.Combat and Target.TTD >= 4 and Target:IsBoss() then
			if Spell.RapidFire:Cast() then
				return true
			end
		end 
		-- Bestial Wrath --
		if Spell.BestialWrath:IsReady() and Pet and Target:IsBoss() and Power >= Setting("Conserve x% Mana") then
			Spell.BestialWrath:Cast(Pet)
		end
		-- Hunter's Mark --
		if Setting("Hunters Mark") and Target.Distance > 8 and Spell.HuntersMark:IsReady() and Target.TTD > 4 and not Target:AuraByID(14324) and not (Target.CreatureType == "Totem") and Power >= Setting("Conserve x% Mana") then
			if Spell.HuntersMark:Cast(Target) then
				return true
			end
		end 		
		-- Auto Attack --	
		if Player.Combat and Target then
			AutoAttack()
		end	
		-- Stings --
		if Sting() then
			return true
		end
		--Concussive Shot --
		if Player:IsTanking(Target) and Target.TTD >= 3 and Spell.ConcussiveShot:IsReady() and Target.Distance >= 8 and Target.Distance <= 35 and Power >= Setting("Conserve x% Mana") then
			if Spell.ConcussiveShot:Cast(Target) then
				return true
			end
		end	
		-- MultiShot --
		if Setting("Multishot") and Target.Distance > 8 and Spell.MultiShot:IsReady() and not (Target.CreatureType == "Totem") and #Target:GetEnemies(8) >= 2 and Power >= Setting("Conserve x% Mana") then
			if Spell.MultiShot:Cast(Target) then
				return true
			end
		end
		-- Volley --
		if Setting("Volley") and Target.Distance > 8 and #Target:GetEnemies(8) >= 2 and Spell.Volley:IsReady() and not (Target.CreatureType == "Totem") and Power >= Setting("Conserve x% Mana") then
			if Spell.Volley:CastGround(Target.PosX, Target.PosY, Target.PosZ) then
				return true
			end
		end
		--Arcane Shot --
		if Target.Distance > 8 and Spell.ArcaneShot:IsReady() and Target.TTD > 4 and not (Target.CreatureType == "Totem") and Power >= Setting("Conserve x% Mana") then
			if Spell.ArcaneShot:Cast(Target) then
				return true
			end
		end
		--Raport Strike --
		if   Target.Distance < 5 and Target.TTD > 2 and not IsAutoRepeatSpell(Spell.AutoShot.SpellName) and Power >= Setting("Conserve x% Mana") then
			if Spell.RaptorStrike:Cast(Target) then
				return true
			end
		end
		-- Wing Clip --
		if Target.Distance < 5 and not Debuff.WingClip:Exist(Target) and not (Target.CreatureType == "Totem") and Power >= Setting("Conserve x% Mana") then
			if Spell.WingClip:Cast(Target) then
				return true
			end	
		end	
		--Mongoose Bite --
		if Target.Distance < 5  and Spell.MongooseBite:IsReady() and Power >= Setting("Conserve x% Mana") then
			if Spell.MongooseBite:Cast(Target) then 
				return true	
			end
		end
	end
end

function Hunter.Rotation()
	Locals()
	-- Check Potions
	Potionuse()
	-- Pet management
	if PetManagement() then
		return true 
	end
	-- Use potions
	if UsePotion() then
		return true 
	end
	-- Check and adjust facing --
	if Setting("Auto Face") and Player.Combat and Target then
		Facing()
	end
	-- Aspects --
	if Aspects() then
		return true
	end
	-- Checking isInstance() to determine Solo or Dungeon Routine
	if inInstance == false or inInstance == nil then
		if Setting("Auto Manage Growl") then
			if IsSpellKnown(6795,true) or IsSpellKnown(14916,true) or IsSpellKnown(14917,true) or IsSpellKnown(14918,true) or IsSpellKnown(14919,true) or IsSpellKnown(14920,true) or IsSpellKnown(14921,true) then
				if growlstate == false then
					print("Turning on Pet Growl(SOLO PLAY)")
					RunMacroText("/petautocasttoggle Growl")
				end
			end
		end
		CombatSolo()
	elseif inInstance == true then
		if instanceType == "party" or instanceType == "raid" then
			if Setting("Auto Manage Growl") then
				if IsSpellKnown(6795,true) or IsSpellKnown(14916,true) or IsSpellKnown(14917,true) or IsSpellKnown(14918,true) or IsSpellKnown(14919,true) or IsSpellKnown(14920,true) or IsSpellKnown(14921,true) then
					if growlstate == true then
						print("Turning off Pet Growl(DUNGEON PLAY)")
						RunMacroText("/petautocasttoggle Growl")
					end
				end
			end
			if CombatDungeon() then
				return true
			end
		end
		if instanceType == "pvp" or instanceType =="arena" or instanceType == nil then
			if CombatSolo() then
				return true
			end
		end
	end
end	 -- End Rotation()